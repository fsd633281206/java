package Day09;

public class Demo7 {
    
    public static int findSecondlargest(int arr[]){
        if (arr == null || arr.length < 2) {
            System.out.println("Invalid input: Array should have at least two elements.");
            return -1;
        }

        int largest = Integer.MIN_VALUE;
        int secondLargest = Integer.MIN_VALUE;

        for (int num : arr) {
            if (num > largest) {
                secondLargest = largest;
                largest = num;
            } else if (num > secondLargest && num != largest) {
                secondLargest = num;
            }
        }

        if (secondLargest == Integer.MIN_VALUE) {
            System.out.println("There is no second largest element in the array.");
            return -1;
        }

        return secondLargest;
    }

    public static void main(String[] args) {
        int arr1[] = {10, 20, 30, 40, 50};
        int arr2[] = {83, 95, 110, 450, 320, 550};
        int arr3[] = {410, 220, 330};
        int arr4[] = {410, 220};
        
        System.out.println(findSecondlargest(arr1));
        System.out.println(findSecondlargest(arr2));
        System.out.println(findSecondlargest(arr3));
        System.out.println(findSecondlargest(arr4));
    }
}